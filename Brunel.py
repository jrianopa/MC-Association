from Configurables import Brunel
from Configurables import L0Conf
Brunel().OutputType = "DST"
Brunel().PrintFreq = -1
Brunel().EvtMax = -1
Brunel().DataType = '2016'  # sets the 2011 configuration of Brunel
Brunel().InputType = "DST" # input has the format digi
Brunel().WithMC    = True   # use the MC truth information in the digi file
Brunel().DDDBtag         = "dddb-20150724"  #Change me to the DB tag you want
Brunel().CondDBtag       = "sim-20161124-2-vc-md100"  #Change me to the DB tag you want
#Brunel().MCLinksSequence = ["L0", "Unpack", "Tr","Calo"]
#Brunel().MCCheckSequence = ""
#Brunel().Detectors = ['Velo', 'PuVeto', 'TT', 'IT', 'OT', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr']
Brunel().Simulation = True
#Brunel().DatasetName = "Test"
Brunel().SplitRawEventInput  = 4.1
Brunel().SplitRawEventOutput = 2.0
#Brunel().VetoHltErrorEvents = False
L0Conf().EnsureKnownTCK=False

from Gaudi.Configuration import *
#from GaudiConf import IOHelper
Brunel().DataType = "2016"
#IOHelper('ROOT').inputFiles(['00040780_00000002_1.mcfilter.xdst'])

from Configurables import OutputStream
writeExtra = True
if writeExtra:
    writerName = "DstWriter"
    dstWriter = OutputStream( writerName )
    dstWriter.ItemList += ["/Event/Rec/Track/Velo#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/FittedHLT1VeloTracks#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/VeloTT#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/ForwardHLT1#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/ForwardHLT2#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/FittedHLT1ForwardTracks#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/Forward#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/Seed#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/Match#1"]
    dstWriter.ItemList += ["/Event/Rec/Track/Downstream#1"]

